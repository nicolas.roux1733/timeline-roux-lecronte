$( document ).ready(function() {
        

        // RECONSTRUCTION DES NOTES DU LOCALSTORAGE
            for (var i = 0; i < localStorage.length; i++)
            {
            var $note = JSON.parse(localStorage.getItem(i))
            $('#timeline').append("<div class='row'><div class='col shadow radius margin white'><div class='margin'><h2><span class='text-bold'>"+ $note['title'] +"</span></h2><p class='text-grey'>"+ $note['desc'] +"</p><br /></div><button id='camera' class='icon-text blue-600 add-photo'><i class='icon ion-camera white'></i>PHOTO</button></div></div>");
            }
            $('#noteForm').click(function(e) {  
                $('#modal').css('opacity', '1');
            });

            $('#close').click(function(e) {  
                $('#modal').css('opacity', '0');
            });
        // RECONSTRUCTION DES NOTES DU LOCALSTORAGE


        var $compt = 0;
        $('#note-validation').click(function(e) { 
            $('#timeline').append("<div class='row'><div class='col shadow radius margin white'><div class='margin'><h2><span class='text-bold'>"+ $('#note-title').val() +"</span></h2><p class='text-grey'>"+ $('#note-desc').val() +"</p><br /></div><button id='camera' class='icon-text blue-600 add-photo'><i class='icon ion-camera white'></i>PHOTO</button></div></div>");
            $('#modal').css('opacity', '0');

            $obj = {
                "title" : $('#note-title').val(),
                "desc" :  $('#note-desc').val()
            }
            localStorage.setItem($compt, JSON.stringify($obj));
            $compt++;
        });

        $('#refreshStorage').click(function(e) { 
           localStorage.clear();
           location.reload();
        });

        $('#camera').click(function(e) { 
            navigator.camera.getPicture(onSuccess, onFail, {
                quality: 50 
            });
        });

});